<?php
 /** Authors: Janne Halonen, Mikko Kokkoniemi, Mari Pääkkönen, Heikki Tauriainen, ryhmä Vitinka 3D, TIK19KM*/
 /** Co-created 16.12.2019 */
session_start();
require_once("account.php");
$database = new Database("localhost", "root", "", "lahiruoka", "utf8");
$connection = $database->connect();

if (!isset($_SESSION['language'])) {
    $database->setDefaultLanguage($connection);
}
else if (isset($_GET['language'])) {
    $_SESSION['language'] = $_GET['language'];
}
else {
    $database->setDefaultLanguage($connection);
}