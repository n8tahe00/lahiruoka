
Janne Halosen CRUD tiedostot ovat:
feedback.php
feedbackGet.php
feedbackUpdate.php
feedbackDelete.php
feedbackSend.php

Mikko Kokkoniemen CRUD tiedostot ovat:
farm.php
farmAdd.php
farmDelete.php
farmEdit.php
farmMyfarm.php
farmUpdate.php

Heikki Tauriaisen CRUD tiedostot ovat: 
userAccount.php
account.php

Mari Pääkkönen CRUD tiedostot ovat:
product.php
productAdd.php
productAddStorage.php
productDeleteStorage.php
productEdit.php
productEditStorage.php
productSave.php
productSaveStorage.php
productShow.php
productUpdate.php
productUpdateStorage.php

Ryhmän yhdessä koodaama kieliversio lisättiin Heikin tekemään käyttäjähallintaan, sekä sivulle language.php