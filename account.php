<?php
/** Author: Heikki Tauriainen, ryhmä Vitinka 3D, TIK19KM*/

class Database {

private $server;
private $username;
private $password;
private $database;
private $charset;

function __construct($server, $username, $password, $database, $charset) {
    $this->server = $server;
    $this->username = $username;
    $this->password = $password;
    $this->database = $database;
    $this->charset = $charset;
}

public function connect() {
    $connection =
        new PDO("mysql:host=$this->server;dbname=$this->database;charset=$this->charset",
            $this->username, $this->password);

    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $connection;
} 

public function haeKayttajat($connection) {
    try {
        $statement = $connection->query(
            "SELECT `kayttajatunnusid`, `asiakasid`, `tuottajaid`, `kayttajatunnus`, `salasana`, `luontipvm`, `muokattu` FROM `kayttajatunnus` WHERE 1");
        if ($statement) {
            $kayttajat = array();
                $i = 0;
                while ($record = $statement->fetch()) {
                    $kayttajat[$i]['kayttajatunnusid'] = $record['kayttajatunnusid'];
                    $kayttajat[$i]['asiakasid'] = $record['asiakasid'];
                    $kayttajat[$i]['tuottajaid'] = $record['tuottajaid'];
                    $kayttajat[$i]['kayttajatunnus'] = $record['kayttajatunnus'];
                    $kayttajat[$i]['salasana'] = $record['salasana'];
                    $kayttajat[$i]['luontipvm'] = $record['luontipvm'];
                    $kayttajat[$i]['muokattu'] = $record['muokattu'];
                    $i++;
                }
                $_SESSION['kayttajat'] = $kayttajat;
                return true;
        }
    }
    catch (Exception $ex) {
        print "<p>Database error: " . $ex->getMessage() . "</p>";
        return false;
    }
}
public function haeTiettyKayttaja($connection) {    
    try {
        $statement = $connection->query(
            "SELECT `kayttajatunnusid`, `asiakasid`, `tuottajaid`, `kayttajatunnus`, `salasana`, `luontipvm`, `muokattu` FROM `kayttajatunnus` WHERE kayttajatunnusid = '" . $_SESSION['kayttajatunnusid'] . "'");
        if ($statement) {
            $kayttajanTiedot = array();
                $i = 0;
                while ($record = $statement->fetch()) {
                    $kayttajanTiedot[$i]['kayttajatunnusid'] = $record['kayttajatunnusid'];
                    $kayttajanTiedot[$i]['asiakasid'] = $record['asiakasid'];
                    $kayttajanTiedot[$i]['tuottajaid'] = $record['tuottajaid'];
                    $kayttajanTiedot[$i]['kayttajatunnus'] = $record['kayttajatunnus'];
                    $kayttajanTiedot[$i]['salasana'] = $record['salasana'];
                    $kayttajanTiedot[$i]['luontipvm'] = $record['luontipvm'];
                    $kayttajanTiedot[$i]['muokattu'] = $record['muokattu'];
                    $i++;
                }
                $_SESSION['kayttajanTiedot'] = $kayttajanTiedot;
                return true;
        }
    }
    catch (Exception $ex) {
        print "<p>Database error: " . $ex->getMessage() . "</p>";
        return false;
    }
}

public function poistaTiettyKayttaja($connection) {    
    try {
        $statement = $connection->query(
            "DELETE FROM `kayttajatunnus` WHERE kayttajatunnusid = '" . $_SESSION['kayttajatunnusid'] . "'");
        if ($statement) {
                return true;
        }
    }
    catch (Exception $ex) {
        print "<p>Database error: " . $ex->getMessage() . "</p>";
        return false;
    }
}

public function paivitaTiettyKayttaja($connection) {    
    try { 
        $sql = "UPDATE `kayttajatunnus` SET `asiakasid`='" . $_SESSION['asiakasid'] . "',`tuottajaid`='" . $_SESSION['tuottajaid'] . "',`kayttajatunnus`='" . $_SESSION['kayttajatunnus'] . "',`salasana`='" . $_SESSION['salasana'] . "' WHERE `kayttajatunnusid`='" . $_SESSION['kayttajatunnusid'] . "'";
        $statement = $connection->query($sql);
        echo "<br>" . $sql;            
        if ($statement) {
                return true;
        }
    }
    catch (Exception $ex) {
        print "<p>Database error: " . $ex->getMessage() . "</p>";
        return false;
    }
}

public function luoUusiKayttaja($connection) {    
    try {
        $statement = $connection->query(
            "INSERT INTO `kayttajatunnus`(`asiakasid`, `tuottajaid`, `kayttajatunnus`, `salasana`) VALUES ('" . $_SESSION['asiakasid'] . "','" . $_SESSION['tuottajaid'] . "','" . $_SESSION['kayttajatunnus'] . "','" . $_SESSION['salasana'] . "')");
        if ($statement) {
                return true;
        }
    }
    catch (Exception $ex) {
        print "<p>Database error: " . $ex->getMessage() . "</p>";
        return false;
    }
}
public function setDefaultLanguage($connection) {
    try {
        $statement = $connection->query(
            "SELECT code FROM app_language ORDER BY orderNo LIMIT 1");
        if ($statement) {
            $record = $statement->fetch();
            $_SESSION['language'] = $record['code'];
            return true;
        }
    }
    catch (Exception $ex) {
        print "<p>Database error: " . $ex->getMessage() . "</p>";
        return false;
    }
}

public function getLanguages($connection) {
    try {
        $statement = $connection->query("SELECT * FROM app_language ORDER BY orderNo");
        if ($statement) {
            $languages = array();
            $i = 0;
            while ($record = $statement->fetch()) {
                $languages[$i]['code'] = $record['code'];
                $languages[$i]['lang'] = $record['lang'];
                $i++;
            }
            $_SESSION['languages'] = $languages;
            return true;
        }
    }
    catch (Exception $ex) {
        print "<p>Database error: " . $ex->getMessage() . "</p>";
        return false;
    }
}

public function getTranslation($connection) {
    try {
        $statement = $connection->query(
            "SELECT * FROM app_translation, app_language 
            WHERE app_translation.code = '". $_SESSION['language'] . "'");
        if ($statement) {
            $record = $statement->fetch();
            $_SESSION['record'] = $record;
            return true;
        }
    }
    catch (Exception $ex) {
        print "<p>Database error: " . $ex->getMessage() . "</p>";
        return false;
    }
}

}
?>