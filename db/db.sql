-- Tänne luodaan kaikki taulujen luontiskriptit, sisällön luontiskriptit sekä päivitykset - uusin ylimmäksi
-- Tuotekuvat sekä palautekuvat erikseen. Kuvataulun funktio uudelleenmietintään?


-- lisätty kaksikielisyys
CREATE TABLE IF NOT EXISTS app_language (
    code CHAR(2) NOT NULL PRIMARY KEY,
    lang VARCHAR(50) NOT NULL,
    orderNo INT(11) NOT NULL
); 

INSERT INTO app_language VALUES ('fi', 'Suomi', 1);
INSERT INTO app_language VALUES ('en', 'English', 2);

CREATE TABLE IF NOT EXISTS app_translation (
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title TEXT NOT NULL,
    deleteaccount TEXT NOT NULL,
    addaccount TEXT NOT NULL,
    modifyaccount TEXT NOT NULL,
    userid TEXT NOT NULL,
    customer TEXT NOT NULL,
    producer TEXT NOT NULL,
    customerid TEXT NOT NULL,
    producerid TEXT NOT NULL,
    username TEXT NOT NULL,
    password TEXT NOT NULL,
    created TEXT NOT NULL,
    modified TEXT NOT NULL,
    linkback TEXT NOT NULL,
    save TEXT NOT NULL,
    code CHAR(2) NOT NULL,
    FOREIGN KEY (code) REFERENCES app_language(code) ON DELETE restrict
);

INSERT INTO app_translation 
    (title, deleteaccount, addaccount, modifyaccount, userid, customer, producer, customerid, producerid, username, password, created, modified, linkback, save, code) VALUES (
    'Käyttäjät',
    'Poista käyttäjätunnus',
    'Lisää uusi käyttäjä',
    'Muokkaa käyttäjän tietoja',
    'Käyttäjätunnusid',
    'Asiakas',
    'Tuottaja',
    'Asiakasid',
    'Tuottajaid',
    'Käyttäjätunnus',
    'Salasana', 
    'Luotu',
    'Muokattu',
    'Takaisin käyttäjälistaukseen',
    'Tallenna',
    'fi'
);

INSERT INTO app_translation 
    (title, deleteaccount, addaccount, modifyaccount, userid, customer, producer, customerid, producerid, username, password, created, modified, linkback, save, code) VALUES (
    'Users',
    'Delete user',
    'Add new user',
    'Modify user',
    'Userid',
    'Customer',
    'Producer',
    'Customerir',
    'Producerid',
    'Username',
    'Password', 
    'Created',
    'Modified',
    'Back to list of users',
    'Save',
    'en'
);


-- lisätty tuoteryhmään 
INSERT INTO tuoteryhma (`tuoteryhmaid`, `tuoteryhmanimi`) VALUES (10006,'Kala');

-- lisätty toimipistetyyppi Toimipiste tauluun
ALTER TABLE toimipiste ADD COLUMN toimipistetyyppi VARCHAR(20) AFTER katuosoite;

-- lisätty käyttäjätunnus tauluun yksi tuottaja
INSERT INTO kayttajatunnus (tuottajaid, kayttajatunnus, salasana) VALUES (1000,'TeHe','Passu123');


-- tietokannan luominen
create database lahiruoka;

use lahiruoka;


-- Luodaan asiakas-taulu

create table asiakas (
    asiakasid INT PRIMARY KEY auto_increment,
    etunimi varchar(80) NOT NULL,
    sukunimi VARCHAR(80) NOT NULL,
    katuosoite VARCHAR(80) NOT NULL,
    puhelin VARCHAR(80) NOT NULL,
    sahkoposti VARCHAR(80) NOT NULL,
    postitoimipaikka VARCHAR(80) NOT NULL,
    postinumero VARCHAR(80) NOT NULL,
    luontipvm TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    muokattu TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    
);

ALTER TABLE asiakas auto_increment = 3000;

-- Luodaan sisältöä asiakastauluun
INSERT INTO `asiakas`(`etunimi`, `sukunimi`, `katuosoite`, `puhelin`, `sahkoposti`, `postitoimipaikka`, `postinumero`) VALUES ('Maija','Poppanen','Tietie12','050-12345567','maija@poppanen.com','Oulu','90250');
INSERT INTO `asiakas`(`etunimi`, `sukunimi`, `katuosoite`, `puhelin`, `sahkoposti`, `postitoimipaikka`, `postinumero`) VALUES ('Teuvo','Testimies','Soratie21','040-9876214','teuvo@testimies.fi','Oulu','90500');
INSERT INTO `asiakas`(`etunimi`, `sukunimi`, `katuosoite`, `puhelin`, `sahkoposti`, `postitoimipaikka`, `postinumero`) VALUES ('Mikko','Mallikas','Pienikatu 50 B 2','044-4004879','mikkomallikas@cmail.com','Liminka','91900');


-- Luodaan tilaus-taulu

create table tilaus (
    tilausid INT PRIMARY KEY auto_increment,
    asiakasid int,
    tilauspvm TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    tapa VARCHAR(2) NOT NULL,
    tila VARCHAR(2) NOT NULL,
    luontipvm TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    muokattu TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (asiakasid) REFERENCES asiakas (asiakasid)
);

ALTER TABLE tilaus auto_increment = 5000;

-- Luodaan sisältöä tilaus-tauluun

INSERT INTO `tilaus`(`tilausid`, `asiakasid`, `tapa`, `tila`) VALUES (5000,3000,'V','M');
INSERT INTO `tilaus`(`tilausid`, `asiakasid`, `tapa`, `tila`) VALUES (5001,3001,'V','T');
INSERT INTO `tilaus`(`tilausid`, `asiakasid`, `tapa`, `tila`) VALUES (5002,3002,'V','T');

-- Luodaan vahvistus-taulu

create table vahvistus (
    vahvistusid INT PRIMARY KEY auto_increment,
    tilausid int,
    pvm TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    vahvistustyyppi VARCHAR(80) NOT NULL,
    luontipvm TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    muokattu TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (tilausid) REFERENCES tilaus (tilausid)
);

ALTER TABLE vahvistus auto_increment = 6000;

-- Luodaan sisältöä vahvistus-tauluun

-- Luodaan kuva-taulu

create table kuva (
    kuvaid INT PRIMARY KEY auto_increment,
    kuvaus varchar(80) NOT NULL,
    url VARCHAR(255) NOT NULL,
    luontipvm TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    muokattu TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE kuva auto_increment = 2000;

-- Luodaan sisältöä kuvatauluun

INSERT INTO `kuva`( `kuvaus`, `url`) VALUES ('Mätä peruna','https://c.pxhere.com/photos/7e/49/age_bacteria_bio_biology_breakfast_bug_colorful_creature-1048631.jpg!d');

-- Luodaan tuottaja-taulu

create table tuottaja (
    tuottajaid INT PRIMARY KEY auto_increment,
    tilatunnus varchar(80) NOT NULL,
    tuottajanimi VARCHAR(80) NOT NULL,
    kuvaid INT,
    luontipvm TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    muokattu TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY(kuvaid) REFERENCES kuva (kuvaid)
);

ALTER TABLE tuottaja auto_increment = 1000;
-- Luodaan sisältöä tuottajatauluun

INSERT INTO `tuottaja`(`tilatunnus`, `tuottajanimi`) VALUES ('teppotila','Tepon maalaisherkut');
INSERT INTO `tuottaja`(`tilatunnus`, `tuottajanimi`) VALUES ('mattijasusu','Matin ja Susannan taimitarha');
INSERT INTO `tuottaja`(`tilatunnus`, `tuottajanimi`) VALUES ('mirka','Mirkan maitotila');
INSERT INTO `tuottaja`(`tilatunnus`, `tuottajanimi`) VALUES ('herrakraak','Kallen kanala');

-- Luodaan toimipiste-taulu

create table toimipiste (
    toimipisteid INT PRIMARY KEY auto_increment,
    nimi VARCHAR(80) NOT NULL,
    katuosoite VARCHAR(80) NOT NULL,
    postinumero VARCHAR(80) NOT NULL,
    postitoimipaikka VARCHAR(80) NOT NULL,
    tuottajaid int,
    luontipvm TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    muokattu TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (tuottajaid) REFERENCES tuottaja (tuottajaid)
);

ALTER TABLE toimipiste auto_increment = 7000;

-- Luodaan sisältöä toimipiste-tauluun
    
    INSERT INTO `toimipiste`(`nimi`, `katuosoite`, `postinumero`, `postitoimipaikka`, `tuottajaid`) VALUES ('Tepon maalaisherkut','Purotie 5',90830,'Haukipudas',1000);
    INSERT INTO `toimipiste`(`nimi`, `katuosoite`, `postinumero`, `postitoimipaikka`, `tuottajaid`) VALUES ('Matin ja Susannan taimitarha','Takaperäntie 10',91300,'Ylikiiminki',1001);
    INSERT INTO `toimipiste`(`nimi`, `katuosoite`, `postinumero`, `postitoimipaikka`, `tuottajaid`) VALUES ('Mirkan maitotila','Koskitie 1',91900,'Liminka',1002);
    INSERT INTO `toimipiste`(`nimi`, `katuosoite`, `postinumero`, `postitoimipaikka`, `tuottajaid`) VALUES ('Kallen kanala','Tarhatie 23',91500,'Muhos',1003);

-- Luodaan kayttajatunnus-taulu

create table kayttajatunnus (
    kayttajatunnusid INT PRIMARY KEY auto_increment,
    asiakasid int,
    tuottajaid int,
    kayttajatunnus VARCHAR(80) NOT NULL,
    salasana VARCHAR(80) NOT NULL,
    luontipvm TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    muokattu TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (asiakasid) REFERENCES asiakas (asiakasid),
    FOREIGN KEY (tuottajaid) REFERENCES tuottaja (tuottajaid)
);

ALTER TABLE kayttajatunnus auto_increment = 4000;

-- Luodaan sisältöä kayttajatunnus-tauluun
INSERT INTO `kayttajatunnus`(`asiakasid`, `kayttajatunnus`, `salasana`) VALUES (3000,'poppis','Salasana12');
INSERT INTO `kayttajatunnus`(`asiakasid`, `kayttajatunnus`, `salasana`) VALUES (3001,'teppis','Eivoiarvata321');
-- Luodaan tuoteryhma-taulu

create table tuoteryhma (

tuoteryhmaid  INT PRIMARY KEY auto_increment, 
    tuoteryhmanimi VARCHAR(80) NOT NULL,
    luontipvm TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    muokattu TIMESTAMP DEFAULT CURRENT_TIMESTAMP
  
); 

ALTER TABLE tuoteryhma auto_increment = 10000;

-- Luodaan sisältöä tuoteryhmä-tauluun
INSERT INTO `tuoteryhma`(`tuoteryhmaid`, `tuoteryhmanimi`) VALUES (10000,'Juurekset');
INSERT INTO `tuoteryhma`(`tuoteryhmaid`, `tuoteryhmanimi`) VALUES (10001,'Liha');
INSERT INTO `tuoteryhma`(`tuoteryhmaid`, `tuoteryhmanimi`) VALUES (10002,'Marjat');
INSERT INTO `tuoteryhma`(`tuoteryhmaid`, `tuoteryhmanimi`) VALUES (10003,'Kasvikset');
INSERT INTO `tuoteryhma`(`tuoteryhmaid`, `tuoteryhmanimi`) VALUES (10004,'Maitotuotteet');
INSERT INTO `tuoteryhma`(`tuoteryhmaid`, `tuoteryhmanimi`) VALUES (10005,'Munat');

-- Luodaan tuote-taulu

create table tuote (
    tuoteid INT PRIMARY KEY auto_increment, 
    tuotenimi VARCHAR(80) NOT NULL,
    hinta DECIMAL(5,2),
    tuottajaid INT,
    kuvaid INT,
    tuoteryhmaid INT,
    luontipvm TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    muokattu TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (tuottajaid) REFERENCES tuottaja (tuottajaid),
    FOREIGN KEY (tuoteryhmaid) REFERENCES tuoteryhma (tuoteryhmaid),
    FOREIGN KEY (kuvaid) REFERENCES kuva (kuvaid)
);

ALTER TABLE tuote auto_increment = 9000;

-- Luodaan sisältöä tuote-tauluun
INSERT INTO `tuote`(`tuotenimi`, `hinta`, `tuottajaid`) VALUES ('Peruna',12,1000);
INSERT INTO `tuote`(`tuoteid`, `tuotenimi`, `hinta`, `tuottajaid`, `tuoteryhmaid`) VALUES (9001,'Lehmänmaito',8,1002,10004);
INSERT INTO `tuote`(`tuoteid`, `tuotenimi`, `hinta`, `tuottajaid`, `tuoteryhmaid`) VALUES (9002,'Mustaherukka',10,1000,10002);
INSERT INTO `tuote`(`tuoteid`, `tuotenimi`, `hinta`, `tuottajaid`, `tuoteryhmaid`) VALUES (9003,'Kananmunakenno',4,1003,10005);

-- Luodaan tilausrivi-taulu

create table tilausrivi (
    tilausriviid INT PRIMARY KEY auto_increment, 
    tilausid int,
    rivinumero int,
    tuoteid int,
    maara int NOT NULL,
    luontipvm TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    muokattu TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (tilausid) REFERENCES tilaus (tilausid),
    FOREIGN KEY (tuoteid) REFERENCES tuote (tuoteid)

);

ALTER TABLE tilausrivi auto_increment = 11000;

-- Luodaan sisältöä tilausrivi-tauluun
INSERT INTO `tilausrivi`(`tilausid`, `rivinumero`, `tuoteid`, `maara`) VALUES (5000,1,9003,2);
INSERT INTO `tilausrivi`(`tilausid`, `rivinumero`, `tuoteid`, `maara`) VALUES (5001,2,9000,10);
INSERT INTO `tilausrivi`(`tilausid`, `rivinumero`, `tuoteid`, `maara`) VALUES (5002,3,9002,7);

-- Luodaan palaute-taulu

create table palaute (
    palauteid INT PRIMARY KEY auto_increment,
    palaute varchar(1000) NOT NULL,
    arvostelu VARCHAR(1000) NOT NULL,
    tuoteid int,
    tuottajaid int,
    tilausid int,
    luontipvm TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    muokattu TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (tuoteid) REFERENCES tuote (tuoteid),
    FOREIGN KEY (tuottajaid) REFERENCES tuottaja (tuottajaid),
    FOREIGN KEY (tilausid) REFERENCES tilaus (tilausid)
);

ALTER TABLE palaute auto_increment = 8000;

-- Luodaan sisältöä palaute-tauluun

INSERT INTO `palaute`(`palaute`, `arvostelu`, `tuoteid`, `tuottajaid`, `tilausid`) VALUES ('Parhaita uusiaperunoita!','*****',9000,1000,5001);
INSERT INTO `palaute`(`palaute`, `arvostelu`, `tuoteid`, `tuottajaid`, `tilausid`) VALUES ('Mustaherukoilla flunssa lähti :)','****',9002,1000,5002);
INSERT INTO `palaute`(`palaute`, `arvostelu`, `tuoteid`, `tuottajaid`, `tilausid`) VALUES ('Kananmunat olivat pilalla..','*',9003,1003,5000);

-- Luodaan tuotevarasto-taulu

create table tuotevarasto (
    tuotevarastoid INT PRIMARY KEY auto_increment, 
    tuoteid int,
    maara int NOT NULL,
    yksikko VARCHAR(80) NOT NULL,
    luontipvm TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    muokattu TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (tuoteid) REFERENCES tuote (tuoteid)

);

ALTER TABLE tuotevarasto auto_increment = 12000;

-- Luodaan sisältöä tuotevarasto-tauluun

INSERT INTO `tuotevarasto`(`tuoteid`, `maara`, `yksikko`) VALUES (9000,100,'kg');
INSERT INTO `tuotevarasto`(`tuoteid`, `maara`, `yksikko`) VALUES (9001,200,'litraa');
INSERT INTO `tuotevarasto`(`tuoteid`, `maara`, `yksikko`) VALUES (9002,50,'kg');
INSERT INTO `tuotevarasto`(`tuoteid`, `maara`, `yksikko`) VALUES (9003,80,'kg');

