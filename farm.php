    <!-- 
    Author: Mikko Kokkoniemi, ryhmä Vitinka 3D, TIK19KM
    -->

    <?php include 'top.php';?>
    

<div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
          <h1 class="mb-0 bread">Tilat</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section contact-section bg-light">
  <div class="container">
      <div class="row d-flex mb-5 contact-info">
      <div class="w-100"></div>
      <div class="col-md-3 d-flex">
    </div>
    
  <!--        
          <div class="info bg-white p-4">
            <p><span>Address:</span> 198 West 21th Street, Suite 721 New York NY 10016</p>
          </div>
      </div>
      <div class="col-md-3 d-flex">
          <div class="info bg-white p-4">
            <p><span>Phone:</span> <a href="tel://1234567920">+ 1235 2355 98</a></p>
          </div>
      </div>
      <div class="col-md-3 d-flex">
          <div class="info bg-white p-4">
            <p><span>Email:</span> <a href="mailto:info@yoursite.com">info@yoursite.com</a></p>
          </div>
      </div>
      <div class="Moro">
          <div class="info bg-white p-4">
            <p><span>Website</span> <a href="#">yoursite.com</a></p>
          </div>
      </div>
col-lg d-flex p-5 col-centered
    </div>
-->  
    <?php require_once 'farmAdd.php'; ?>
    <div class="row block-9 align-items-center">
      <div class="col-md col-centered">
        <form action="farmAdd.php" method="POST" class="bg-white p-3 contact-form">
          <div class="form-group">
          <input type="text" class="form-control" name="nimi" placeholder="Tilan nimi :">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="katuosoite" placeholder="Tilan katuosoite :">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="postinumero" placeholder="Postinumero :">
          </div>
          <div class="form-group">
          <input type="text" class="form-control" name="postitoimipaikka" placeholder="Postitoimipaikka :">
          </div>
          <div class="col-md form-group d-flex p-5 items-align-center">
            <input type="submit" value="Lisää tila" name="lisaatila" class="btn btn-primary py-3 px-5">
          </div>
          
        </form>
        <div class="col-md form-group d-flex p-5 items-align-center">
          <a href="farmMyfarm.php"><input type="submit" value="Omat tilasi" name="omattilat" class="btn btn-primary py-3 px-5">
          </div>
      
      </div>
  </div>

      <!--<div class="col-md-6 d-flex">
          <div id="map" class="bg-white"></div>
      </div>-->
    </div>
  </div>
</section>

<?php include 'footer.php';?>
