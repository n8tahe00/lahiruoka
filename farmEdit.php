    <!-- 
    Author: Mikko Kokkoniemi, ryhmä Vitinka 3D, TIK19KM
    -->

<?php
    $con = mysqli_connect("localhost", "root", "", "lahiruoka");

    if(!$con) {
        die("Tietokantaan ei saatu yhteyttä");
    }

    $toimipisteid = $_GET['GetID'];
    $query = " select * from toimipiste where toimipisteid='".$toimipisteid."'";
    $result = mysqli_query($con,$query);

    while($row=mysqli_fetch_assoc($result)) {
                                $toimipisteid = $row['toimipisteid'];
                                $nimi = $row['nimi'];
                                $katuosoite = $row['katuosoite'];
                                $postinumero = $row['postinumero'];
                                $postitoimipaikka = $row['postitoimipaikka'];
    }


?>


<?php include 'top.php';?>


<div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
          <h1 class="mb-0 bread">Tilat</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section contact-section bg-light">
  <div class="container">
      <div class="row d-flex mb-5 contact-info">
      <div class="w-100"></div>
      <div class="col-md-3 d-flex">
      <?php require_once 'farmAdd.php'; ?>
    <div class="row block-9 align-items-center">
      <div class="col-md col-centered">

        <form action="farmUpdate.php?ID=<?php echo $toimipisteid ?>" method="POST" class="bg-white p-3 contact-form">
          <div class="form-group">
          <input type="text" class="form-control" name="nimi" placeholder="Tilan nimi :" value="<?php echo $nimi ?>">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="katuosoite" placeholder="Tilan katuosoite :" value="<?php echo $katuosoite ?>">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="postinumero" placeholder="Postinumero :" value="<?php echo $postinumero ?>">
          </div>
          <div class="form-group">
          <input type="text" class="form-control" name="postitoimipaikka" placeholder="Postitoimipaikka :" value="<?php echo $postitoimipaikka ?>">
          </div>
          <div class="col-md form-group d-flex p-5 items-align-center">
            <input type="submit" value="Muokkaa" name="muokkaa" class="btn btn-primary py-3 px-5">
          </div>
        </form>
      
      </div>
  </div>
    </div>
    </div>
</section>

<?php include 'footer.php';?>