    <!-- 
    Author: Mikko Kokkoniemi, ryhmä Vitinka 3D, TIK19KM
    -->

<?php
require_once("farmAdd.php");
$query = " select * from toimipiste";
$result = mysqli_query($con,$query);


?>


<?php include 'top.php';?>


<div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
          <h1 class="mb-0 bread">Omat tilasi</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section contact-section bg-light">
  <div class="container">
      <div class="row d-flex mb-5 contact-info">
      <div class="w-100"></div>
      <div class="col-md-3 d-flex">
    </div>

    <div class="row block-9">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 m-auto">
                    <table class="table table-bordered">
                        <tr>
                            <td>Toimipiste</td>
                            <td>Nimi</td>
                            <td>Katuosoite</td>
                            <td>Postinumero</td>
                            <td>Postitoimipaikka</td>
                            <td>Muokkaa</td>
                            <td>Poista</td>
                        </tr>
                
                        <?php
                            while($row=mysqli_fetch_assoc($result)) {
                                $toimipisteid = $row['toimipisteid'];
                                $nimi = $row['nimi'];
                                $katuosoite = $row['katuosoite'];
                                $postinumero = $row['postinumero'];
                                $postitoimipaikka = $row['postitoimipaikka'];
                            
                        ?>
                            <tr>
                                <td><?php echo $toimipisteid ?></td>
                                <td><?php echo $nimi ?></td>
                                <td><?php echo $katuosoite ?></td>
                                <td><?php echo $postinumero ?></td>
                                <td><?php echo $postitoimipaikka ?></td>
                                <td><a href="farmEdit.php?GetID=<?php echo $toimipisteid ?>">Muokkaa</a></td>
                                <td><a href="farmDelete.php?Del=<?php echo $toimipisteid ?>">Poista</a></td>
                            </tr>

                            <?php
                                    }


                            ?>

                    </table>

                </div>

            </div>
        </div>


    </div>

    </div>
</section>

<?php include 'footer.php';?>