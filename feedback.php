

    <?php include 'top.php';?>


<div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
          <h1 class="mb-0 bread">Palautteet</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section contact-section bg-light">
  <div class="container">
      <div class="row d-flex mb-5 contact-info">
      
      <h1>Palaute</h1>
     <!-- <div class="col-md-3 d-flex">
          
          <div class="info bg-white p-4">
            <p><span>Address:</span> 198 West 21th Street, Suite 721 New York NY 10016</p>
          </div>
      </div>
      <div class="col-md-3 d-flex">
          <div class="info bg-white p-4">
            <p><span>Phone:</span> <a href="tel://1234567920">+ 1235 2355 98</a></p>
          </div>
      </div>
      <div class="col-md-3 d-flex">
          <div class="info bg-white p-4">
            <p><span>Email:</span> <a href="mailto:info@yoursite.com">info@yoursite.com</a></p>
          </div>
      </div>
      <div class="">
          <div class="info bg-white p-4">
            <p><span>Website</span> <a href="#">yoursite.com</a></p>
          </div>
      </div>

    </div>
-->
    
     
    <div class="row block-9">
      <div class="col-md-6 order-md-last d-flex">
        <form action="feedbacksend.php" method="post">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Nimi">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Sähköposti">
          </div>
          <div class="form-group">
            <input type="text" id="arvostelu" class="form-control" placeholder="Arvostelu">
          </div>
          <div class="form-group">
            <textarea id="palaute" cols="70" rows="5" class="form-control" placeholder="Viesti"></textarea>
          </div>
          <div class="form-group">
            <input type="submit" value="Lähetä" class="btn btn-primary py-3 px-5"></br>
          </div>
        </form>
        <div class="form-group">
        <a class="btn btn-primary py-3 px-5" href="feedbackget.php" role="button">Avaa palautteet</a>
        </div><br>

       
    </div>
  </div>
</section>


<?php include 'footer.php';?>
