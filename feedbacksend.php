<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "lahiruoka";
$palaute = "palaute";
$arvostelu = "arvostelu";
$tuoteid = "tuoteid";
$tuottajaid = "tuottajaid";
$tilausid = "tilausid";
$luontipvm = "luontipvm";
$muokattu = "muokattu";


try {
    $conn = new PDO(
        "mysql:host=$servername;dbname=$dbname",
        $username,
        $password
    );

    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare("INSERT INTO palaute ($palaute, $arvostelu) 
VALUES (:arvostelu, :palaute)");
    $stmt->bindParam(':arvostelu', $_REQUEST['arvostelu']);
    $stmt->bindParam(':palaute', $_REQUEST['palaute']);
    $stmt->execute();


    echo "Palautteesi on tallennettu. Kiitos!";
} catch (PDOException $e) {
    echo "Tallennuksessa tapahtui virhe. Lähetä uudelleen.  Virhekoodi:  " . $e->getMessage();
}
$conn = null;

?>
<a href="index.php"> Palaa</a>