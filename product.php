<?php include 'top.php';
/*
* Author: Mari Pääkkönen, ryhmä Vitinka 3D, TIK19KM
*/ 
session_start();

// alustetaan tunnus ja salasana muuttujat
$_SESSION["tunnus"]="";
$_SESSION["salasana"]="";

?>


<div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <h1 class="mb-0 bread">Tuotteet</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section contact-section bg-light">
  <div class="container">
    
    <h3>Kirjautuminen</h3>
    <div class="row block-9">
      <div class="col-md-6 order-md-last d-flex">
        <form action="productShow.php" class="bg-white p-5 contact-form" method="POST">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Käyttäjätunnus" name="tunnus">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Salasana" name="salasana">
          </div>
          <div class="form-group">
            <input type="submit" value="Kirjaudu" class="btn btn-primary py-3 px-5">
          </div>
          
        </form>

      </div>
    </div>

  </div>
</section>

<?php include 'footer.php'; ?>