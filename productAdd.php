<?php include 'top.php';
/*
* Author: Mari Pääkkönen, ryhmä Vitinka 3D, TIK19KM
*/ 
session_start();
$tuottajaid = $_SESSION['tuottajaid'];
$_SESSION["kirjautuminen"]= 1;

?>


<div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <h1 class="mb-0 bread">Tuotteet</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section contact-section bg-light">
  <div class="container">

<h3>Tuotteen lisääminen</h3>
    <div class="row block-9">
      <div class="col-md-6 order-md-last d-flex">

        <form id="ProductInfo" action="productSave.php" class="bg-white p-5 contact-form" method="POST">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Tuote" name="tuote">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Hinta" name="hinta">
          </div>
          <div class="form-group">
            <input type="radio" name="tuoteryhma" value="10000" checked> Juurekset<br>
            <input type="radio" name="tuoteryhma" value="10002"> Marjat<br>
            <input type="radio" name="tuoteryhma" value="10003"> Kasvikset<br>
            <input type="radio" name="tuoteryhma" value="10004"> Maitotuotteet<br>
            <input type="radio" name="tuoteryhma" value="10005"> Munat<br>
            <input type="radio" name="tuoteryhma" value="10001"> Liha<br> 
            <input type="radio" name="tuoteryhma" value="10006"> Kala<br>
          </div>
          <div class="form-group">
            <input type="submit" value="Lisää" class="btn btn-primary py-3 px-5">
          </div>
        </form>

      </div>
    </div>


  </div>
</section>

<?php include 'footer.php'; ?>