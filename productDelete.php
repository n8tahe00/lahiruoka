<?php include 'top.php';
/*
* Author: Mari Pääkkönen, ryhmä Vitinka 3D, TIK19KM
*/ 
session_start();
$tuottajaid = $_SESSION['tuottajaid'];
$_SESSION["kirjautuminen"]= 1;
$tuoteid = $_GET['tuoteid'];
$tunnus = $_SESSION['tunnus'];
$salasana = $_SESSION['salasana'];


$servername = "localhost";
$username = "root";
$password = "";
$dbname = "lahiruoka";

try {
    $connection = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8", $username, $password);
    // set the PDO error mode to exception
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // begin the transaction
    $connection->beginTransaction();
    
    $sql = "DELETE FROM tuote WHERE tuoteid = :tuoteid"; 

    $query = $connection->prepare($sql);
    $query->bindValue(":tuoteid", $tuoteid, pdo::PARAM_INT);
    $query->execute();

                  
}
catch(PDOException $error)
{
    // rollback eli perutaan transaction
    $connection->rollback();

echo "Tietokantavirhe " . $error->getMessage();
}

// suljetaan yhteys
$connection= null;

?>

<div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <h1 class="mb-0 bread">Tuotteet</h1>

        <h3>Tuote poistettu tuottajan listalta</h3>
            <a href ='productShow.php'>Takaisin</a>

      </div>
    </div>
  </div>
</div>
<?php include 'footer.php'; ?>