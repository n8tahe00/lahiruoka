<?php include 'top.php';
/*
* Author: Mari Pääkkönen, ryhmä Vitinka 3D, TIK19KM
*/ 
session_start();
$tuottajaid = $_SESSION['tuottajaid'];
$_SESSION["kirjautuminen"]= 1;
$tuoteid = $_GET['tuoteid'];


$tunnus = $_SESSION['tunnus'];
$salasana = $_SESSION['salasana'];

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "lahiruoka";

try {
        $connection = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8", $username, $password);
        // set the PDO error mode to exception
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
        // begin the transaction
        $connection->beginTransaction();
      
        $sql = "SELECT * FROM tuote WHERE tuoteid = $tuoteid";
        $query = $connection->query($sql);
        $query->setFetchMode(PDO::FETCH_OBJ);

        while($row = $query->fetch()) {
          $tuotenimi = $row->tuotenimi;
          $hinta = $row->hinta;
        }
        
        $_SESSION["paivitatuote"]= $tuoteid;
           
}
catch(PDOException $error){
        // rollback eli perutaan transaction
        $connection->rollback();

    print "Tietokantavirhe " . $error->getMessage();
}

    // suljetaan yhteys
    $connection= null;

  ?>
<!-- ?> -->


<div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <h1 class="mb-0 bread">Tuotteet</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section contact-section bg-light">
  <div class="container">

<h3>Tuotteen muokkaaminen</h3>
    <div class="row block-9">
      <div class="col-md-6 order-md-last d-flex">

        <form id="ProductInfo" action="productUpdate.php?ID=<?php echo $tuoteid ?>" class="bg-white p-5 contact-form" method="POST">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Tuote" name="tuote" value="<?php print $tuotenimi; ?>">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Hinta" name="hinta" value="<?php print $hinta; ?>">
          </div>
          <div class="form-group">
            <input type="radio" name="tuoteryhma" value="10000" checked> Juurekset<br>
            <input type="radio" name="tuoteryhma" value="10002"> Marjat<br>
            <input type="radio" name="tuoteryhma" value="10003"> Kasvikset<br>
            <input type="radio" name="tuoteryhma" value="10004"> Maitotuotteet<br>
            <input type="radio" name="tuoteryhma" value="10005"> Munat<br>
            <input type="radio" name="tuoteryhma" value="10001"> Liha<br> 
            <input type="radio" name="tuoteryhma" value="10006"> Kala<br>
          </div>
          <div class="form-group">
            <input type="submit" value="Tallenna" name="paivitaTuote" class="btn btn-primary py-3 px-5">
          </div>
        </form>

      </div>
    </div>


  </div>
</section>

<?php include 'footer.php'; ?>