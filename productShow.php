<?php include 'top.php';
/*
* Author: Mari Pääkkönen, ryhmä Vitinka 3D, TIK19KM
*/ 
session_start();

?>


<div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <h1 class="mb-0 bread">Tuotteet</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section contact-section bg-light">
  <div class="container">
    
  <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "lahiruoka";

  // haetaan muuttujat
  $tunnus = $_SESSION['tunnus'];
  $salasana = $_SESSION['salasana'];

  
  if(strlen($tunnus)== 0){
    $tunnus = filter_input(INPUT_POST, 'tunnus', FILTER_SANITIZE_STRING);
  }

  if (strlen($salasana)== 0){
    $salasana = filter_input(INPUT_POST, 'salasana', FILTER_SANITIZE_STRING);
  }
  

    try {
        $connection = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8", $username, $password);
        // set the PDO error mode to exception
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
        // begin the transaction
        $connection->beginTransaction();
      
        // tarkistetaan löytyykö syötettyjä tunnuksia vastaavaa tuottajaa
        $stmt = $connection->prepare("SELECT tuottajaid FROM kayttajatunnus WHERE kayttajatunnus = :tunnus AND salasana = :salasana");
        $stmt->bindValue(":tunnus", $tunnus, PDO::PARAM_STR);
        $stmt->bindValue(":salasana", $salasana, PDO::PARAM_STR);
        $stmt->execute();

        
         
        $tulos = $stmt->rowCount();
        
        // jos tunnusta ei löydy
        if ($tulos <= 0){
            print "<p>Käyttäjätunnus tai salasana ei täsmää, kirjaudu uudelleen</p>";
            print 
            '<div class="form-group">
            <button class="btn btn-primary py-3 px-5" onclick="goBack()">Takaisin kirjautumiseen</button>
            </div>';
           
        } // jos tunnus löytyy
        else {
          $_SESSION["kirjautuminen"]= 1;
          // haetaan kaikki tuottajan tuotteet
          $tuottajaid = $stmt->fetchColumn();
          $stmt = $connection->prepare("SELECT T.tuoteid, tuotenimi, maara, yksikko, hinta, tuoteryhmanimi 
          FROM tuote as T LEFT JOIN  tuotevarasto as TV ON T.tuoteid = TV.tuoteid 
          LEFT JOIN tuoteryhma as TR ON T.tuoteryhmaid = TR.tuoteryhmaid 
          WHERE T.tuottajaid = '$tuottajaid'");

          $_SESSION["tuottajaid"]= $tuottajaid;
          $_SESSION["tunnus"]= $tunnus;
          $_SESSION["salasana"]=$salasana;          

          $stmt->execute();
          $stmt-> setFetchMode(PDO::FETCH_OBJ);

          $result = $stmt->fetchAll();
          
          // tulostetaan kaikki tuottajan tuotteet tauluun
          //print $_SESSION['tuottajaid'];
          
          print "<div class='table-responsive'>";
          print "<table class='table table-strped table-sm'>";
          print "<thead>";
          print "<tr>";
          print "<th>Tuote</th>
                <th>Määrä</th>
                <th></th>
                <th>Hinta</th>
                <th>Tuoteryhmä</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>";
          print "</tr>";
          print "</thead>";

          foreach ($result as $row){
              print "<tr>";
              print "<td>$row->tuotenimi</td>";
              print "<td>$row->maara</td>";
              print "<td>$row->yksikko</td>";
              print "<td>$row->hinta</td>";
              print "<td>$row->tuoteryhmanimi</td>";

              $tuoteid = $row->tuoteid;
              print "<td><a href ='ProductAddStorage.php?tuoteid=$tuoteid'>Lisää varasto</a></td>";
              print "<td><a href ='ProductEditStorage.php?tuoteid=$tuoteid'>Muokkaa varastoa</a></td>";
              print "<td><a href ='ProductDeleteStorage.php?tuoteid=$tuoteid'>Poista varasto</a></td>";
              print "<td><a href ='ProductEdit.php?tuoteid=$tuoteid'>Muokkaa tuotetta</a></td>";
              // print "<td><a href ='ProductDelete.php?tuoteid=$tuoteid'>Poista tuote</a></td>";  tietokanta yhteyksien vuoksi tätä ei ole otettu käyttöön
              print "</tr>";

          }
          print "</table>";
          print "</div>";
          print "<p></p>";
          print "<div class='form-group'>
                <button onclick='goProductAdd()' class='btn btn-primary py-3 px-5'>Lisää uusi tuote</button>
                </div>";

          $connection->commit();

        } 
                   
    }
    catch(PDOException $error)
    {
        // rollback eli perutaan transaction
        $connection->rollback();

    print "Tietokantavirhe " . $error->getMessage();
    }

    // suljetaan yhteys
    $connection= null;

  ?>

 <!-- painikkeiden toiminto funktiot -->
  <script>
    
    function goBack(){
    window.history.back();
    }

    function goProductAdd(){
     window.location.href = "ProductAdd.php";  
    }
    
  </script> 

</section>

<?php include 'footer.php'; ?>