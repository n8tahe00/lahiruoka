<?php include 'top.php';
/*
* Author: Mari Pääkkönen, ryhmä Vitinka 3D, TIK19KM
*/ 
session_start();
$tuottajaid = $_SESSION['tuottajaid'];

if (isset($_POST['paivitaTuote'])){
    $tuoteid = $_GET['ID'];
}

$tuote = filter_input(INPUT_POST, 'tuote', FILTER_SANITIZE_STRING);
$hinta = filter_input(INPUT_POST, 'hinta', FILTER_SANITIZE_NUMBER_INT);
$tuoteryhmaid = $_POST['tuoteryhma'];

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "lahiruoka";

try {
    $connection = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8", $username, $password);
    // set the PDO error mode to exception
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // begin the transaction
    $connection->beginTransaction();

    //lisätään tuotetiedot
    $query = $connection->prepare("UPDATE tuote SET tuotenimi = '$tuote', hinta = $hinta, tuoteryhmaid = $tuoteryhmaid WHERE tuoteid = $tuoteid");
   // echo $query;
    $query->execute();
    $connection->commit();
    
}

catch(PDOException $error)
{
    // rollback eli perutaan transaction
    $connection->rollback();

echo "Tietokantavirhe " . $error->getMessage();
}

// suljetaan yhteys
$connection= null;
?>
<div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <h1 class="mb-0 bread">Tuotteet</h1>
      </div>
    </div>
  </div>
</div>
<section class="ftco-section contact-section bg-light">
  <div class="container">

<h3>Tuoteen tiedot päivitetty</h3>
<a href ='productShow.php'>Takaisin tuotteisiin</a>

</div>
</section>
<?php include 'footer.php'; ?>