    <?php 
    /** Author: Heikki Tauriainen, ryhmä Vitinka 3D, TIK19KM*/
    include 'top.php';
    include_once("Language.php");
    require_once("account.php");
    $database = new Database("localhost", "root", "", "lahiruoka", "utf8");
    $connection = $database->connect();
    if ($database->getLanguages($connection)) {
      if ($database->getTranslation($connection)) {
    ?>


    <div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<h1 class="mb-0 bread"><?php echo $_SESSION['record']['title'] ?></h1>
          </div>
        </div>
      </div>
    </div>

    <section class="bg-light">
      <div class="container">
        <div class="row d-flex mb-5">
          <?php
          for ($i = 0; $i < count($_SESSION['languages']); $i++) {
              $language = $_SESSION['languages'][$i];
              echo "<a href=?language=" . $language['code'] . ">" . 
                  $language['lang'] . "</a>";
              if (($i + 1) < count($_SESSION['languages'])) {
                  echo "<span>&nbsp;|&nbsp;</span>";
              }
          }
          ?>
        </div>
      </div
    </section>
  
    <?php
    // Noudetaan lomakkeella tehdyt muutokset
    if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') === "POST") {
        // Tallennetaan uuden käyttäjätunnuksen tiedot
        if (filter_input(INPUT_POST, "tallenna", FILTER_SANITIZE_STRING) === "1") {
          echo "tallennetaan uuden tiedot";
          $_SESSION['asiakasid'] = filter_input(INPUT_POST, "asiakasid", FILTER_SANITIZE_NUMBER_INT);
          $_SESSION['tuottajaid'] = filter_input(INPUT_POST, "tuottajaid", FILTER_SANITIZE_NUMBER_INT);
          $_SESSION['kayttajatunnus'] = filter_input(INPUT_POST, "kayttajatunnus", FILTER_SANITIZE_STRING);
          $_SESSION['salasana'] = filter_input(INPUT_POST, "salasana", FILTER_SANITIZE_SPECIAL_CHARS);
          $database = new Database("localhost", "root", "", "lahiruoka", "utf8");
              $connection = $database->connect();
              if ($database->luoUusiKayttaja($connection)) {
                echo "<script> location.replace('userAccount.php?lisatty=1&language=" . $language['code'] . "'); </script>";
              }
        }
        else {
        // tallennetaan olemassaolevaan tunnukseen tehdyt muutokset
        echo "tallennetaan kantaan muutokset";
        $_SESSION['kayttajatunnusid'] = filter_input(INPUT_POST, "kayttajatunnusid", FILTER_SANITIZE_NUMBER_INT);
        $_SESSION['asiakasid'] = filter_input(INPUT_POST, "asiakasid", FILTER_SANITIZE_NUMBER_INT);
        $_SESSION['tuottajaid'] = filter_input(INPUT_POST, "tuottajaid", FILTER_SANITIZE_NUMBER_INT);
        $_SESSION['kayttajatunnus'] = filter_input(INPUT_POST, "kayttajatunnus", FILTER_SANITIZE_STRING);
        $_SESSION['salasana'] = filter_input(INPUT_POST, "salasana", FILTER_SANITIZE_SPECIAL_CHARS);
        $database = new Database("localhost", "root", "", "lahiruoka", "utf8");
              $connection = $database->connect();
              if ($database->paivitaTiettyKayttaja($connection)) {
                echo "<script> location.replace('userAccount.php?paivitetty=1&language=" . $language['code'] . "'); </script>";
              }
        }
    }
    // poimitaan URL-parametreinä annetut käyttäjätunnusid:t
    else if (filter_input(INPUT_GET, "kayttajatunnusid", FILTER_SANITIZE_NUMBER_INT) <> "") {
      // Poistetaan valittu käyttäjä
      if (filter_input(INPUT_GET, "poista", FILTER_SANITIZE_STRING) === "1") {
        $kayttajatunnusid = filter_input(INPUT_GET, "kayttajatunnusid", FILTER_SANITIZE_NUMBER_INT);
        $_SESSION['kayttajatunnusid'] = $kayttajatunnusid;
        $database = new Database("localhost", "root", "", "lahiruoka", "utf8");
              $connection = $database->connect();
              if ($database->poistaTiettyKayttaja($connection)) {
                echo "<script> location.replace('userAccount.php?poistettu=1&language=" . $language['code'] . "'); </script>";
              }
      }
      // Käyttäjätietojen muokkauslomake
      else {
      ?>
      <section class="ftco-section contact-section bg-light">
      <div class="container">
      	<div class="row d-flex mb-5">
           <div class="col-md-3 d-flex">
            <h2><?php echo $_SESSION['record']['modifyaccount']?></2>
          </div>
          <section class="ftco-section contact-section bg-light">
          <div class="container">
          <div class='table-responsive'>
          <table class='table table-strped table-sm'>
      <?php
      $kayttajatunnusid = filter_input(INPUT_GET, "kayttajatunnusid", FILTER_SANITIZE_NUMBER_INT);
      $_SESSION['kayttajatunnusid'] = $kayttajatunnusid;
      $database = new Database("localhost", "root", "", "lahiruoka", "utf8");
            $connection = $database->connect();
            if ($database->haeTiettyKayttaja($connection)) 
                echo "<form action='userAccount.php' method='POST'>";
                for ($i = 0; $i < count($_SESSION['kayttajanTiedot']); $i++) {
                  $kayttajanTiedot = $_SESSION['kayttajanTiedot'][$i];
                  echo "<input type='hidden' name='kayttajatunnusid' value='" . $kayttajanTiedot['kayttajatunnusid'] . "'>";
                  echo "<input type='hidden' name='language' value='" . $language['code'] . "'";
                  echo "<div>" . $_SESSION['record']['customer'] . ":</div><div><input name='asiakasid' type='text' value='" . $kayttajanTiedot['asiakasid'] . "'></div>";
                  echo "<div>" . $_SESSION['record']['producer'] . ":</div><div><input name='tuottajaid' type='text' value='" . $kayttajanTiedot['tuottajaid'] . "'></div>";
                  echo "<div>" . $_SESSION['record']['username'] . ":</div><div><input name='kayttajatunnus' type='text' value='" . $kayttajanTiedot['kayttajatunnus'] . "'></div>";
                  echo "<div>" . $_SESSION['record']['password'] . ": </div><div><input name='salasana' type='text' value='" . $kayttajanTiedot['salasana'] . "'></div>";
                  echo "<div>" . $_SESSION['record']['created'] . ": </div><div><input name='luontipvm' type='text' disabled value='" . $kayttajanTiedot['luontipvm'] . "'></div>";
                  echo "<div>" . $_SESSION['record']['modified'] . ": </div><div><input name='muokattu' type='text' disabled value='" . $kayttajanTiedot['muokattu'] . "'></div>";
                  echo "<div><button class='btn btn-primary py-3 px-5'>" . $_SESSION['record']['save'] . "</button></div>";
                echo "</form>";
                }
            echo "<div><p><a href='userAccount.php'>" . $_SESSION['record']['linkback'] . "</a></p></div>";
                
            }
          }
    
    else {
      // uuden käyttäjän lisääminen
      if (filter_input(INPUT_GET, "lisaa", FILTER_SANITIZE_STRING) === "1") {
         ?>
        <div class="container">
      	<div class="row d-flex mb-5">
           <div class="col-md-3 d-flex">
            <h2><?php echo $_SESSION['record']['addaccount']?></2>
          </div>
          <section class="ftco-section contact-section bg-light">
          <div class="container">
          <div class='table-responsive'>
          <table class='table table-strped table-sm'>
        <?php
          echo "<form action='userAccount.php' method='POST'>";
          echo "<input type='hidden' name='tallenna' value='1'>";
          echo "<input type='hidden' name='language' value='" . $language['code'] . "'";
          echo "<div>" . $_SESSION['record']['customer'] . ":</div><div><input title='Vinkki kokeile arvoa 3000' name='asiakasid' type='text' value=''></div>";
          echo "<div>" . $_SESSION['record']['producer'] . ":</div><div><input title='Vinkki kokeile arvoa 1000' name='tuottajaid' type='text' value=''></div>";
          echo "<div>" . $_SESSION['record']['username'] . ":</div><div><input name='kayttajatunnus' type='text' value=''></div>";
          echo "<div>" . $_SESSION['record']['password'] . ": </div><div><input name='salasana' type='text' value=''></div>";
          echo "<div><button class='btn btn-primary py-3 px-5'>" . $_SESSION['record']['save'] . "</button></div>";
          echo "</form>";
          echo "<div><p><a href='userAccount.php'>" . $_SESSION['record']['linkback'] . "</a></p></div>";
      }
      // muutoin listataan kaikki käyttäjätunnukset
      else {
      ?>
      <section class="ftco-section contact-section bg-light">
        <div class="container">
          <div class="row d-flex mb-5">
            <div class="col-md-3 d-flex">
              <h2><?php echo $_SESSION['record']['title']?></2>
            </div>
            <?php
            if (filter_input(INPUT_GET, "lisatty", FILTER_SANITIZE_STRING) === "1") {
              echo "<div>Uusi käyttäjä lisätty onnistuneesti</div>";
            }
            if (filter_input(INPUT_GET, "poistettu", FILTER_SANITIZE_STRING) === "1") {
              echo "<div>Käyttäjä poistettu onnistuneesti</div>";
            }
            if (filter_input(INPUT_GET, "paivitetty", FILTER_SANITIZE_STRING) === "1") {
              echo "<div>Käyttäjän tiedot päivitetty onnistuneesti</div>";
            }
            ?>
            <section class="ftco-section contact-section bg-light">
            <div class="container">
            <div class='table-responsive'>
            <table class='table table-strped table-sm'>
              <thead>
                <tr>
                  <th><?php echo $_SESSION['record']['userid'] ?></th>
                  <th><?php echo $_SESSION['record']['customerid'] ?></th>
                  <th><?php echo $_SESSION['record']['producerid'] ?></th>
                  <th><?php echo $_SESSION['record']['username'] ?></th>
                  <th><?php echo $_SESSION['record']['password'] ?></th>
                  <th><?php echo $_SESSION['record']['created'] ?></th>
                  <th><?php echo $_SESSION['record']['modified'] ?></th>
                  <th></th>
                </tr>
              </thead>
              <?php
              $database = new Database("localhost", "root", "", "lahiruoka", "utf8");
              $connection = $database->connect();
              if ($database->haeKayttajat($connection)) {
                for ($i = 0; $i < count($_SESSION['kayttajat']); $i++) {
                  $kayttaja = $_SESSION['kayttajat'][$i];
                  echo "<tr>";
                  echo "<td><a href=?kayttajatunnusid=" . $kayttaja['kayttajatunnusid'] . "&language=" . $language['code'] . ">" . $kayttaja['kayttajatunnusid'] . "</a></td>";
                  echo "<td>" . $kayttaja['asiakasid'] . "</td>";
                  echo "<td>" . $kayttaja['tuottajaid'] . "</td>";
                  echo "<td>" . $kayttaja['kayttajatunnus'] . "</td>";
                  echo "<td>" . $kayttaja['salasana'] . "</td>";
                  echo "<td>" . $kayttaja['luontipvm'] . "</td>";
                  echo "<td>" . $kayttaja['muokattu'] . "</td>";
                  echo "<td><a href='userAccount.php?poista=1&kayttajatunnusid=" . $kayttaja['kayttajatunnusid'] . "&language=" . $language['code'] . "'>" . $_SESSION['record']['deleteaccount'] . "</a></td>";
                  echo "</tr>";
                }
              }
              ?>
              <tr><td colspan=7><button  onclick='javascript:lisaaKayttaja();' class='btn btn-primary py-3 px-5'><?php echo $_SESSION['record']['addaccount']?></button></tr>
          </div>
          </table>
        </div>
      </div>
      <?php
      }
      // käyttäjien listaus päättyy tähän
    }
  }
}
    ?>
       </div>
      </div>
    </section>
    <script>
    function lisaaKayttaja () {
      window.location.href = "userAccount.php?lisaa=1&language=<?php echo $language['code'];?>";
    }
    </script>

    <?php include 'footer.php';?>
    